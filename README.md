minetest mod creeper
====================

MOBS for Creeper mob into the game which ignites and explodes

## Information
--------------

This mod must be named `mobs_creeper` and is a fork and continuation of `creeper`.
from TenPlus1, a hostile mob that random drops include gunpowder, iron or coal lumps.

![screenshot.png](screenshot.png)

Creepers are dark green color.  Once they spot you they will march toward you. 
When they get close to you they start charging and after 3 seconds explode. 
If you are very close to the creeper when it explodes you are sure to die so 
make sure to fight them from distance.

## Technical information
------------------------

Creepers spawn  during the night and at low light levels, 
if Ethereal mod is active then they appear in the mushroom biomes instead.

First Creeper was a Minetest mod created by Rui to introduce creeper 
into the game, for the purpose of some explosive nuisance. 
Later Kai Gerd Müller introduce new mod more elaborate but bad performance 
for servers, and finally TenPlus1 adapted  from mineclone game.

This mod was improved to follow mobs namespace and be more like origina one, 
their namespace were changed from  `creeper` to `mobs_creeper`.

### Depends

* default
* mobs_redo

### know issues

* it explotion will destroy everything, and not only soft/weal block/nodes
* ia are not so smart like in other games
