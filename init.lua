
-- Creeper by Davedevils (from his subgame MineClone)

mobs:register_mob("mobs_creeper:creeper", {
	type = "monster",
	passive = false,
	attack_type = "explode",
	explosion_radius = 1,
	explosion_timer = 3,
	reach = 3,
	damage = 21,
	hp_min = 30,
	hp_max = 40,
	armor = 90,
	collisionbox = {-0.4, -1, -0.4, 0.4, 0.8, 0.4},
	visual = "mesh",
	mesh = "mobs_tree_monster.b3d",
	textures = {
		{"mobs_creeper.png"},
	},
	blood_texture = "mobs_creeper_inv.png",
	makes_footstep_sound = false,
	sounds = {
		--random = "mobs_treemonster",
		attack = "tnt_ignite",
		explode = "tnt_explode",
		fuse = "tnt_ignite",
	},
	walk_velocity = 1,
	run_velocity = 3,
	jump = true,
	view_range = 10,
	fear_height = 3,
	drops = {
		{name = "tnt:gunpowder", chance = 1, min = 0, max = 2},
		{name = "default:iron_lump", chance = 5, min = 0, max = 2},
		{name = "default:coal_lump", chance = 3, min = 0, max = 3},
	},
	water_damage = 2,
	lava_damage = 15,
	light_damage = 1,
	animation = {
		stand_start = 0,		stand_end = 24,
		walk_start = 25,		walk_end = 47,
		run_start = 48,			run_end = 62,
		punch_start = 48,		punch_end = 62,
		speed_normal = 15,		speed_run = 15,
	},
})

local spawn_on = "group:stone"

if minetest.get_modpath("ethereal") then
	spawn_on = {"group:stone", "ethereal:mushroom_dirt"}
end

mobs:spawn({
	name = "mobs_creeper:creeper",
	nodes = {spawn_on},
	min_light = 0,
	max_light = 5,
	chance = 12000,
	min_height = 0,
	day_toggle = false,
})

mobs:register_egg("mobs_creeper:creeper", "Creeper", "mobs_creeper_inv.png", 1)

mobs:alias_mob("mobs:creeper", "mobs_creeper:creeper") -- compatibility
